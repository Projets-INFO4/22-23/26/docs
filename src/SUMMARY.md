# Summary

[Introduction](index.md)
[Modules](architecture/modules.md)

# Architecture

- [Architecture classique d'un compilateur](architecture/classic.md)
- [Vue d'ensemble](architecture/overview.md)
- [Lexeur (logos)](architecture/logos.md)
- [Parseur (rowan + nom + ungrammar)](architecture/parsing.md)
- [Queries (yéter)]()

# Type-checking

1. [Type-checking](typechecking/types.md)
2. [Vérification de l'initialisation](typechecking/initialization.md)

# Extended code

- [Génération](ec/generation.md)
- [Simulation]()

# Annexes

- [Glossaire](annexes/glossary.md)
- [Sources](annexes/sources.md)
